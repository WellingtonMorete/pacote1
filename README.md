# pacote1

> Pacote de Exemplo


## Instalação

```bash
composer require wellington/pacote1

```

## API

```php

namespace Wellington\Pacote1;


Class Exemplo {
    /**
     * 
     * Retorna o nome
     * */
    function nome();
}

```

## Licença

MIT